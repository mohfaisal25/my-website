$.ajax({
    type: 'GET',
    url: '/book/data/',
    success: function (data) {
        var dataBuku = '';
        console.log(data);
        $.each(data.items, function(index, item) {
            var volume = item.volumeInfo;
            var image = volume.imageLinks.thumbnail;
            var title = volume.title;
            var author_list = volume.authors;
            var author = '';
            $.each(author_list, function(i, value) {
                author += value + ',' + '<br>'
            });
            var slice_author = author.slice(0, author.length-5);
            var description = volume.description;
            dataBuku += '<tr>';
            dataBuku += '<th scope="row">' + (index+1) + '</th>';
            dataBuku += '<td>' + '<img src=' + image + '></td>';
            dataBuku += '<td>' + title + '</td>';
            dataBuku += '<td>' + slice_author + '</td>';
            dataBuku += '<td>' + description + '</td>';
            dataBuku += '</tr>';
        });
        $('#my_book_table').append(dataBuku);
    }
});