from django.urls import path
from . import views

urlpatterns = [
    path('data/', views.get_api, name='api'),
    path('', views.index, name='index'),
]