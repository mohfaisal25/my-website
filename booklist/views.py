from django.shortcuts import render
from django.http import JsonResponse
import requests

response = {}
def index(request):
    response['title'] = 'Books'
    return render(request, 'booklist.html', response)

def get_api(request, value = "quilting"):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + value
    json_page = requests.get(url).json()
    return JsonResponse(json_page)

