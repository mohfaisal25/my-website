from django import forms

class StatusForm(forms.Form):
    status_attrs = {
        'type' : 'text',
        'placeholder' : 'Write your status here...',
        'class' : 'form-control'
    }

    status = forms.CharField(label='Status', max_length=300, required=True, empty_value='none',
                                widget=forms.Textarea(attrs=status_attrs))