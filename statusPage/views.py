from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import StatusModel

# Create your views here.
response = {}
def status(request):
    status = StatusForm()    
    status_data = StatusModel.objects.all()
    response['status_form'] = status
    response['status_data'] = status_data
    status_val = request.POST.get('status')

    if (request.method == 'POST'):
        StatusModel.objects.create(status=status_val)
        return HttpResponseRedirect("/status/")

    return render(request, 'status.html', response)
