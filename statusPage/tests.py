from django.test import TestCase, Client
from django.urls import resolve
from .views import status
from .models import StatusModel
from .forms import StatusForm
from django.utils import timezone
from django.http import HttpRequest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.


class StatusPageUnitTest(TestCase):
    def test_status_page_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

    def test_status_page_using_status_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, status)

    def test_model_can_create_new_status(self):
        # Creating a new status
        new_status = StatusModel.objects.create(status='baik-baik saja',
            date=timezone.now().__str__())
        # Retrieving all available status
        counting_all_available_status = StatusModel.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    # def test_form_validation_for_blank_items(self):
    #     form = StatusForm(data={'status': ''})
    #     self.assertFalse(form.is_valid())
    #     self.assertEqual(form.errors['status'], ["This field is required."])

    def test_status_page_return_httpresponse(self):
        request = HttpRequest()
        response = status(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Halo, apa kabar?', html_response)

    def test_can_save_POST_request(self):
        response = self.client.post(
            '/status/', data={'status': 'Saya baik-baik saja'})
        self.assertEqual(response.status_code, 302)

    def test_form_status_input_has_placeholder_and_css_classes(self):
        form = StatusForm()
        self.assertIn('id="id_status"', form.as_p())
        self.assertIn('class="form-control', form.as_p())


class StatusPageFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(StatusPageFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.implicitly_wait(3)
        self.selenium.quit()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/status')
        time.sleep(3)
        # find the form element
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        status.send_keys('coba coba')
        time.sleep(3)

        # submitting the form
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        self.assertIn('coba coba', self.selenium.page_source)
