from django.db import models
from django.utils import timezone

# Create your models here.
class Schedule(models.Model):
	name = models.CharField(max_length=255)
	date = models.DateTimeField(default=timezone.now)
	location = models.CharField(max_length=255)
	category = models.CharField(max_length=255)
