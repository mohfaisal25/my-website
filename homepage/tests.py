from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Schedule
from .forms import Todo_Form
from django.utils import timezone
from django.http import HttpRequest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class PortofolioWebsiteUnitTest(TestCase):   
    def test_homepage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_homepage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_model_can_create_new_schedule(self):
        # Creating a new schedule
        new_status = Schedule.objects.create(name="budi", date="2018-05-08 08:30:02", location="depok", category="biasa")
        # Retrieving all available status
        counting_all_available_status = Schedule.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_homepage_return_httpresponse(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello World !', html_response)



