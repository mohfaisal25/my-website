# Generated by Django 2.1.1 on 2018-10-04 07:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0002_auto_20181004_1408'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='time',
            field=models.CharField(max_length=25),
        ),
    ]
