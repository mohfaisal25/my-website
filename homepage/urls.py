from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('guestbook/', views.guestbook, name='guestbook'),
    path('my-story/', views.stories, name='my-story'),
    path('saveschedule/', views.saveschedule, name='saveschedule'),
    path('addschedule/', views.addschedule, name='addschedule'),
	path('result/', views.result, name='result'),
	path('delete-data/', views.delete_all_data, name='delete-data')  
]