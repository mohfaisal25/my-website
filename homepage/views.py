from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from .forms import Todo_Form
from .models import Schedule


# Create your views here.
response = {}
def index(request):
	schedule = Schedule.objects.all()
	response['schedule'] = schedule
	
	return render(request, 'index.html', response)

def addschedule(request):
	response['schedule_form'] = Todo_Form

	return render(request, 'schedule.html', response)

def stories(request):
	return render(request, 'my-story.html')

def guestbook(request):
	return render(request, 'guestbook.html')

def saveschedule(request):
	form = Todo_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
		response['date'] = request.POST['date'] if request.POST['date'] != "" else "Anonymous"
		response['location'] = request.POST['location']
		response['category'] = request.POST['category']
		schedule = Schedule(name=response['name'], date=response['date'],
							location=response['location'], category=response['category'])
		schedule.save()
		html = 'schedule.html'
		return render(request, html, response)
	else:

		return HttpResponseRedirect('/')

def result(request):
	result = Schedule.objects.all()
	response['result'] = result
	html = 'result.html'
	return render(request, html, response)

def delete_all_data(request):
	Schedule.objects.all().delete()
	html = 'schedule.html'
	#return render(request, html, response)
	return HttpResponseRedirect('/')
	