'''
Form untuk mencatat jadwal kegiatan.
Meliputi: hari, tanggal, jam, nama kegiatan,
tempat, dan kategori.
'''

from django import forms


class Todo_Form(forms.Form):
	todo_attrs = {
		'type': 'text',
        'class': 'form-control',
        'placeholder':'nama kegiatan'
	}

	loc_attrs = {
		'type': 'text',
        'class': 'form-control',
        'placeholder':'tempat kegiatan'
	}

	category_attrs = {
		'type': 'text',
        'class': 'form-control',
        'placeholder':'jenis kategori'
	}

	date_attrs = {
		'type': 'date',
		'class': 'form-control',
		'placeholder': 'yyyy-mm-dd'
	}

	'''time_attrs = {
		'type' : 'text',
		'class' : 'form-control',
		'placeholder' : 'hh:mm:ss'
	}'''
	name = forms.CharField(label='Nama Kegiatan', required=True, max_length=255, empty_value='none',
				widget=forms.TextInput(attrs=todo_attrs))
	date = forms.DateTimeField(required=True, widget=forms.DateTimeInput(attrs=date_attrs))
	#time  = forms.CharField(label='Waktu Kegiatan', required=True, max_length=25, empty_value='none',
			#	widget=forms.TextInput(attrs=time_attrs))
	location = forms.CharField(label='Tempat', required=True, max_length=255, empty_value='none',
				widget= forms.TextInput(attrs=loc_attrs))
	category = forms.CharField(label='Kategori', required=True, max_length=255, empty_value='none',
				widget= forms.TextInput(attrs=category_attrs))



