// Tema 1
$(document).ready(function () {
    $("button#themes1").click(function () {
        $("#navbar").css("background-color", "#8600b3");
        $(".jumbotron").css("background-color", "#bb33ff");
        $(".about").css("background-color", "#cc66ff");
        $(".education").css("background-color", "#dd99ff");
        $(".experience").css("background-color", "#e6b3ff");
        $(".skills").css("background-color", "#eeccff");
        $(".contact").css("background-color", "#f7e6ff");
        $("#accordion h3").css("background", "#8600b3");
    })
});
// Tema 2
$(document).ready(function () {
    $("button#themes2").click(function () {
        $("#navbar").css("background-color", "#009900");
        $(".jumbotron").css("background-color", "#4dff4d");
        $(".about").css("background-color", "#80ff80");
        $(".education").css("background-color", "#99ff99");
        $(".experience").css("background-color", "#b3ffb3");
        $(".skills").css("background-color", "#ccffcc");
        $(".contact").css("background-color", "#e6ffe6");
        $("#accordion h3").css("background", "#009900");
    })
});

$(document).ready(function() {
    $("#accordion").accordion({
        icons: { "header": "ui-icon-plus", "activeHeader": "ui-icon-minus"},
        animate: 200,
        collapsible: true, active: 'none',
    })
});


var myVar;
function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("content").style.display = "block";
}