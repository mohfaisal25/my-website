from django import forms

class RegistrationForm(forms.Form):
    name_attrs = {
        'type' : 'text',
        'class' : 'form-control',
        'placeholder' : 'Masukkan nama Anda',
        'id' : 'name-input'
    }

    email_attrs = {
        'type' : 'email',
        'class' : 'form-control',
        'placeholder' : 'example@domain.com',
        'id' : 'email-input'
    }

    password_attrs = {
        'type' : 'password',
        'class' : 'form-control',
        'placeholder' : 'password minimal 8 karakter',
        'id' : 'password-input'
    }

    name = forms.CharField(label='Name', max_length=50, required=True, widget=forms.TextInput(attrs=name_attrs))
    email = forms.EmailField(label='Email', required=True, widget=forms.EmailInput(attrs=email_attrs))
    password = forms.CharField(label='Password', required=True, max_length=50, min_length=8, widget=forms.PasswordInput(attrs=password_attrs))
