from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .models import User
from .forms import RegistrationForm

# Create your views here.
response = {}
def registration(request):
    reg_form = RegistrationForm
    response['registration_form'] = reg_form
    return render(request, 'registration.html', response)

def create_user(request):
    form = RegistrationForm(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']

        obj = User(name=name, email=email, password=password)
        obj.save();
        return JsonResponse({'success': True})
    return JsonResponse({'success' : False})


def validate_account(request):
    email_arr = []
    data = {}
    for e in User.objects.all():
        email_arr.append(e.email)
    data["email"] = email_arr
    return JsonResponse(data)
